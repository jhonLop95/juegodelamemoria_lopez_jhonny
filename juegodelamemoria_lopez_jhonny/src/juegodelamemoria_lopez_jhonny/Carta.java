/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juegodelamemoria_lopez_jhonny;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author jhon
 */
public class Carta {
    private String nombre;
    private String rutaCarta;
    private ImageView imageCarta=new ImageView();
    private boolean estaCubierta=true;
    Image image;
    
    //Constructur de la clase que solo recive dos parametros , el nombre y la ruta.
    public Carta(String nombre, String rutaCarta) {
        this.nombre = nombre;
        this.rutaCarta = rutaCarta;
        image=new Image(Memoria.RUTA_CARTA_CUBIERTA);
        imageCarta.setFitHeight(140);
        imageCarta.setFitWidth(100);
        imageCarta.setImage(image);
    }
    //Getters y Setters de la clase

    public ImageView getImageCarta() {
        return imageCarta;
    }

    public void setImageCarta(ImageView imageCarta) {
        this.imageCarta = imageCarta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRutaCarta() {
        return rutaCarta;
    }

    public void setRutaCarta(String rutaCarta) {
        this.rutaCarta = rutaCarta;
    }

    public boolean isEstaCubierta() {
        return estaCubierta;
    }

    public void setEstaCubierta(boolean estaCubierta) {
        this.estaCubierta = estaCubierta;
    }
   public void setImage(String rutaCarta){
       image=new Image(rutaCarta);
        imageCarta.setFitHeight(140);
        imageCarta.setFitWidth(100);
        imageCarta.setImage(image);
   }
   
   
   
	
   
}
