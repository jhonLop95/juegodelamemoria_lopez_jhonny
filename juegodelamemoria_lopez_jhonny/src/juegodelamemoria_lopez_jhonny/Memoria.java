/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juegodelamemoria_lopez_jhonny;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author jhon
 */
public class Memoria extends Application {

    //Atributos del juego
    //Root del tablero de cartas
    private GridPane root;
    private VBox rootFinal, rootInicio, rootSalirFinal, rootGanar;
    private HBox rootSalir, rootGanar2, rootGanar3;
    private AnchorPane cronometro;
    private Scene scene, scene1;
    //Botones del juego
    private Button si, no, jugar, salir, carta1, carta2, carta3, carta4, carta5, carta6, carta7, carta8, carta9, carta10, carta11, carta12, carta13, carta14, carta15, carta16;
    private Stage primaryStage;
    private Label mensaje1, tiempo1, t, intentos, numero;
    private int conteo, segundos = 0, minutos = 0, horas = 0, intento = 0;
    private double tiempo_transcurrido = 0;
    public static final int FILAS = 4;
    public static final int COLUMNAS = 4;
    public static final String RUTA_CARTA_CUBIERTA = "/Imagenes/Fondo.png";
    private ArrayList<Carta> cartas;
    private ArrayList<Button> botones;
    Musica musica = new Musica();
    ArrayList<Integer> numeros ;
    ArrayList<Integer> indices ;

    //Metodo que genera numeros aletorios hasta el numero dado.
    public int obtenerAleatorio(int limiteHasta) {
        Random numeroAleatorio = new Random();
        int numero = numeroAleatorio.nextInt(limiteHasta);
        return numero;
    }

    public void llenarCartas() {
        root = new GridPane();
        cargarCartas();

        botones = new ArrayList<>();
        carta1 = new Button();
        carta2 = new Button();
        carta3 = new Button();
        carta4 = new Button();
        carta5 = new Button();
        carta6 = new Button();
        carta7 = new Button();
        carta8 = new Button();
        carta9 = new Button();
        carta10 = new Button();
        carta11 = new Button();
        carta12 = new Button();
        carta13 = new Button();
        carta14 = new Button();
        carta15 = new Button();
        carta16 = new Button();
        botones.add(carta1);
        botones.add(carta2);
        botones.add(carta3);
        botones.add(carta4);
        botones.add(carta5);
        botones.add(carta6);
        botones.add(carta7);
        botones.add(carta8);
        botones.add(carta9);
        botones.add(carta10);
        botones.add(carta11);
        botones.add(carta12);
        botones.add(carta13);
        botones.add(carta14);
        botones.add(carta15);
        botones.add(carta16);
        //Recorremos los botones y añadimos la imagen de cada carta
        for (int i = 0; i < botones.size(); i++) {
            botones.get(i).setGraphic(cartas.get(i).getImageCarta());
        }
        //Recorremos el root del tablero y añadimos los botones.
        int recorrido = 0;
        for (int i = 0; i < FILAS; i++) {
            for (int j = 0; j < COLUMNAS; j++) {
                root.add(botones.get(recorrido), j, i);
                recorrido = recorrido + 1;
            }
        }
        //Inicializamos el label de mensaje de tiempo transcurrido
        t = new Label("Tiempo transcurrido:");
        //Enceramos el label de tiempo
        tiempo1 = new Label("00:00:00");
        segundos = 0; minutos = 0; horas = 0; intento = 0;
        //Inicializamos el label de mensaje de intentos
        intentos = new Label("Intentos:");
        //Inicializamos el label del numero de intentos
        numero = new Label("0");
        
        //Capturamos los eventos de los botones con el metodo chekBotones
        chekBotones();
        //Enceramos los valores de las posiciones del par de cartas destapadas
        numeros = new ArrayList<Integer>();
        indices = new ArrayList<Integer>();
        numeros.add(-1);
        numeros.add(-1);
        root.setVgap(10);
        root.setHgap(10);
        root.setPadding(Insets.EMPTY);
        root.setAlignment(Pos.CENTER);
        rootFinal = new VBox(10);
        rootFinal.getChildren().addAll(cronometro(), root);
        Tiempo hiloTiempo = new Tiempo();
        (new Thread(hiloTiempo)).start();
        scene = new Scene(rootFinal);

        String css = Memoria.class.getResource("Login.css").toExternalForm();
        scene.getStylesheets().add(css);

        primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("Juego De Memoria");
        primaryStage.getIcons().add(new Image("/Imagenes/Logo.png"));
        primaryStage.show();
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent event) {
                musica.stop();

            }
        });

    }

    public void cargarCartas() {
        //Creo lista  con los nombres de los personajes 
        List<String> nombres = new ArrayList<>();
        nombres.add("Mario");
        nombres.add("Mario");
        nombres.add("Donkey Kong");
        nombres.add("Donkey Kong");
        nombres.add("Link");
        nombres.add("Link");
        nombres.add("Samus");
        nombres.add("Samus");
        nombres.add("Yoshi");
        nombres.add("Yoshi");
        nombres.add("Kirby");
        nombres.add("Kirby");
        nombres.add("Fox");
        nombres.add("Fox");
        nombres.add("Pikachu");
        nombres.add("Pikachu");
        //LIsta de las cartas a llenar
        cartas = new ArrayList<>();
        //Lista de numeros aleatorios
        List numerosAleatorios = new ArrayList();
        //Bucle para llenar las cartas en la lista.
        for (int i = 0; i < (FILAS * COLUMNAS); i++) {
            int llave = 0;
            while (llave == 0) {
                int numeroAleatorio = obtenerAleatorio(FILAS * COLUMNAS);
                if (numerosAleatorios.contains(numeroAleatorio)) {
                    //Si contiene en nuemero aletorio no realizar nada
                } //Si no lo coentie es nuevo , ingresarlo a la lista y crear la carta con ese numero
                else {
                    numerosAleatorios.add(numeroAleatorio);

                    cartas.add(new Carta(nombres.get(numeroAleatorio), "Imagenes/Carta" + (numeroAleatorio + 1) + ".png"));

                    llave = 1;

                }
            }

        }
    }

    //Realizamos la captura de dar clic en cada boton.
    public void chekBotones() {
        carta1.setOnAction((ActionEvent event) -> {
            //Reproducimos la musica al pulsar el boton
            musica.playCarta();
            //Ingresamos el index del boton en el metodo logica.
            logica(0);
            //Verificamos si se terminaron de destapar todos los botones con el metodo ganar
            ganar();

        });
        carta2.setOnAction((ActionEvent event) -> {
            musica.playCarta();
            logica(1);
            ganar();
        });
        carta3.setOnAction((ActionEvent event) -> {
            musica.playCarta();
            logica(2);
            ganar();
        });
        carta4.setOnAction((ActionEvent event) -> {
            musica.playCarta();
            logica(3);;
            ganar();
        });
        carta5.setOnAction((ActionEvent event) -> {
            musica.playCarta();
            logica(4);
            ganar();
        });
        carta6.setOnAction((ActionEvent event) -> {
            musica.playCarta();
            logica(5);
            ganar();
        });
        carta7.setOnAction((ActionEvent event) -> {
            musica.playCarta();
            logica(6);
            ganar();
        });
        carta8.setOnAction((ActionEvent event) -> {
            musica.playCarta();
            logica(7);
            ganar();
        });
        carta9.setOnAction((ActionEvent event) -> {
            musica.playCarta();
            logica(8);
            ganar();
        });
        carta10.setOnAction((ActionEvent event) -> {
            musica.playCarta();
            logica(9);
            ganar();
        });
        carta11.setOnAction((ActionEvent event) -> {
            musica.playCarta();
            logica(10);
            ganar();
        });
        carta12.setOnAction((ActionEvent event) -> {
            musica.playCarta();
            logica(11);
            ganar();
        });
        carta13.setOnAction((ActionEvent event) -> {
            musica.playCarta();
            logica(12);
            ganar();
        });
        carta14.setOnAction((ActionEvent event) -> {
            musica.playCarta();
            logica(13);
            ganar();
        });
        carta15.setOnAction((ActionEvent event) -> {
            musica.playCarta();
            logica(14);
            ganar();
        });
        carta16.setOnAction((ActionEvent event) -> {
            musica.playCarta();
            logica(15);
            ganar();
        });
    }

    //Metodo inicio de lanza el stage con los botones para iniciar el juego.
    public void inicio() {
        
        
        //inicializamos el root Inicio y damos una separacion de 20 pixeles
        rootInicio = new VBox(20);
        //Inicializamos los botones
        jugar = new Button("Jugar");
        salir = new Button("Salir");
        //Seteamos un tamaño minimo.
        jugar.setMinSize(150, 30);
        salir.setMinSize(150, 30);
        //Capturamos el evento de dar clic en jugar 
        jugar.setOnAction((ActionEvent event) -> {
            //Llenamos las cartas con el metodo llenar cartas
            llenarCartas();
            //reproduciomos la musica de pulsar el boton
            musica.playIntro();
            //reproduciomos la musica del tablero de juego 
            musica.play();
        });
        //capturamos el evento de dar clic en el boton salir
        salir.setOnAction((ActionEvent event) -> {
            //Reproducimos la musica de pulsar el boton
            musica.playIntro();
            //Llamamos al Stage de confirmacion de salir con el metodo salir
            salir();
        });
        //Llenamos el root inicio con los botones salir y jugar
        rootInicio.getChildren().addAll(jugar, salir);
        //Centramos el root
        rootInicio.setAlignment(Pos.CENTER);
        //Inializamos la escena y le damos un tamaño minimo
        scene = new Scene(rootInicio, 300, 100);
        //creamos un String con la dirreccion del archivo .css que le dara el estilo a la escena
        String css = Memoria.class.getResource("Login.css").toExternalForm();
        //Cargamos la el archivo de estilo css en la escena.
        scene.getStylesheets().add(css);
        //Inicalizamos el estage
        primaryStage = new Stage();
        // seteamos la escena en el stage
        primaryStage.setScene(scene);
        //Ponemos el titulo en el estage 
        primaryStage.setTitle("Juego De Memoria");
        primaryStage.getIcons().add(new Image("/Imagenes/Logo.png"));
        //Mostramos el stage
        primaryStage.show();
    }

    public void logica(int i) {
        if(numeros.get(0)!=-1 && numeros.get(1)==-1 && numeros.get(0)!=i){
            if(cartas.get(i).isEstaCubierta()){
               cartas.get(i).setEstaCubierta(false);
               cartas.get(i).setImage(cartas.get(i).getRutaCarta());
               numeros.set(1, i);
            }
        }
        if(numeros.get(0)==-1 && numeros.get(1)==-1){
            if(cartas.get(i).isEstaCubierta()){
                
               cartas.get(i).setEstaCubierta(false);
               cartas.get(i).setImage(cartas.get(i).getRutaCarta());
               
               numeros.set(0, i);
            }
        }
        if(numeros.get(0)!=-1 && numeros.get(1)!=-1){
            if(cartas.get(numeros.get(0)).getNombre().equals(cartas.get(numeros.get(1)).getNombre()) && !indices.contains(numeros.get(0)) && !indices.contains(numeros.get(1))){
                System.out.println(numeros.get(0)+":"+numeros.get(1));
                indices.add(numeros.get(0));
                indices.add(numeros.get(1));
                numeros.set(0, -1);
                numeros.set(1, -1);
                intento++;
            }
            else{
                System.out.println(numeros.get(0)+":"+numeros.get(1));
                Transcion cart1=new Transcion(cartas.get(numeros.get(0)));
                Transcion cart2=new Transcion(cartas.get(numeros.get(1)));
                
                cart1.start();
                cart2.start();
                
                numeros.set(0, -1);
                numeros.set(1, -1);
                intento++;
                
            }
        }
    }

    public void salir() {
        //Inicializamos los root a usar con una separacion de 20 pixeles
        rootSalir = new HBox(20);
        rootSalirFinal = new VBox(20);
        //Inicializamos los botones
        si = new Button("Si");
        no = new Button("No");
        //Damos un tamaño minimo a los botones
        si.setMinSize(60, 30);
        no.setMinSize(60, 30);
        // Inicializamos el label que tiene la pregunta de confirmacion.
        mensaje1 = new Label("¿Esta segudo que queire salir?");
        //capturamos el evento de dar clic en si 
        si.setOnAction((ActionEvent event) -> {
            // reproduciomos la musica  de los botones
            musica.playIntro();
            //realizamos la salida del juego.
            System.exit(0);
        });

        //Agregamos los botones a un preroot 
        rootSalir.getChildren().addAll(si, no);
        //Centramos los  botones
        rootSalir.setAlignment(Pos.CENTER);
        //Añadimos el label con la pregunta y el preroot con los botones
        rootSalirFinal.getChildren().addAll(mensaje1, rootSalir);
        //centramos 
        rootSalirFinal.setAlignment(Pos.CENTER);
        //Añadimos el root a la escena y damos un tamaño minimo de 250x100 pixeles
        Scene scene = new Scene(rootSalirFinal, 300, 100);
        //Inicializamos el Stage 
        Stage stage = new Stage();
        //cargamos el string con la direccion del archivo de estilo .css
        String css = Memoria.class.getResource("Login.css").toExternalForm();
        //Añadimos el estilo a la escena almacenada en el archivo .css
        scene.getStylesheets().add(css);
        //Seteamos la escena en el Stage
        stage.setScene(scene);
        //Capturamos el evento de dar clic sobre el boton no
        no.setOnAction((ActionEvent event) -> {
            //Reproducimos la musica al dar clic sobre el boton
            musica.playIntro();
            //Cerramos el stage.
            stage.close();
        });
        //Ponemos el titulo del juego
        stage.setTitle("Juego de Memoria");
        stage.getIcons().add(new Image("/Imagenes/Logo.png"));
        //Mostramos el stage.
        stage.show();

    }

    @Override
    public void start(Stage primaryStage) throws Excepciones {
        inicio();

    }

    public static void main(String[] args) {
        launch(args);
    }
//Metodo que retorna un anchorpane con los label  del cronometro y el numero de intentos 

    public AnchorPane cronometro() {
        
        //Inicializamos el anchorpane
        cronometro = new AnchorPane();
        //Agregamos los label al anchorpane 
        cronometro.getChildren().addAll(t, tiempo1, intentos, numero);
        //Posicionamos el mensaje del label de mensaje de tiempo en el pixel 20
        AnchorPane.setLeftAnchor(t, 20.0);
        //Posicionamos el mensaje del label de tiempo en el pixel 180
        AnchorPane.setLeftAnchor(tiempo1, 180.0);
        //Posicionamos el mensaje del label de intento 
        AnchorPane.setLeftAnchor(intentos, 300.0);
        //Posicionamos el mensaje del label de cada intento
        AnchorPane.setLeftAnchor(numero, 380.0);
        //retornamos el anchorpane cronometro
        return cronometro;
    }

    public void ganar() {

        int conteo = 0;
        //Verificamos si todas las castas estan destapadas
        for (Carta c : cartas) {
            if (!c.isEstaCubierta()) {
                conteo++;
            }

        }
        //Si todas estan destapadas, lanzamos el stage de felicitaciones
        if (conteo == 16) {
            primaryStage.close();
            rootGanar = new VBox(10);
            rootGanar2 = new HBox(10);
            rootGanar3 = new HBox(10);
            Label mensaje1 = new Label("Felicidades Ganaste!!!");
            rootGanar2.getChildren().addAll(t, tiempo1);
            rootGanar2.setAlignment(Pos.CENTER);
            rootGanar3.getChildren().addAll(intentos, numero);
            rootGanar3.setAlignment(Pos.CENTER);
            rootGanar.getChildren().addAll(mensaje1, rootGanar2, rootGanar3);
            rootGanar.setAlignment(Pos.CENTER);

            scene1 = new Scene(rootGanar, 300, 300);
            String css = Memoria.class.getResource("Login.css").toExternalForm();
            scene1.getStylesheets().add(css);
            Stage stage1 = new Stage();
            stage1.setScene(scene1);
            stage1.setTitle("Juego De Memoria");
            stage1.getIcons().add(new Image("/Imagenes/Logo.png"));
            stage1.show();
            stage1.setOnCloseRequest(new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent event) {
                musica.stop();
                
            }
        });
            //Detenemos la musica del tablero de juego.
            musica.stop();
            //Damos play a la musica de ganar
            musica.playGanar();

        }

    }
    //Clase interna del tiempo. 

    public class Tiempo implements Runnable {

        private boolean parar = false;

        @Override
        public void run() {
            //Inicamos un bucle while que se para cuando todas las cartas estan destapadas
            while (!parar) {
                conteo = 0;
                if (cartas != null) {
                    for (Carta c : cartas) {
                        if (!c.isEstaCubierta()) {
                            conteo++;
                        }
                    }
                    if (conteo == 16) {
                        parar = true;

                    }
                }
                Platform.runLater(() -> {
                    //Actualiza la informacion del label de tiempo
                    tiempo1.setText(String.format("%02d", horas) + ":" + String.format("%02d", minutos) + ":" + String.format("%02d", segundos));
                    numero.setText("" + intento);
                    //Suma un segundo cada vez 
                    segundos++;
                    //si alcanzan los 59 segundos , se enceran y se añade un minuto-.
                    if (segundos == 60) {
                        segundos = 0;
                        minutos++;
                    }
                    //Si alcanzan los 59 minutos , se enceran y se añade una hora. 
                    if (minutos == 60) {
                        minutos = 0;
                        horas++;
                    }
                });
                try {
                    //Duerme el hilo un segundo para el conteo del tiempo
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Memoria.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }

    }
}
