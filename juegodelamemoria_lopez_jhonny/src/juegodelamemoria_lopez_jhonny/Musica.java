/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juegodelamemoria_lopez_jhonny;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

/**
 *
 * @author jhon
 */
public class Musica {
    //cargo la musica de fondo en los botones
    Media mediaIntro=new Media(Memoria.class.getResource("/musica/SuperSmashBros-043.wav").toExternalForm());
    Media mediaFondo=new Media(Memoria.class.getResource("/musica/Música de Super Mario Bros. 3 - Tema Principal 1.mp3").toExternalForm());
    MediaPlayer c;
    //creo un constructor vacio    
   public Musica(){
       
   }
   public void play(){
       mediaFondo=new Media(Memoria.class.getResource("/musica/Música de Super Mario Bros. 3 - Tema Principal 1.mp3").toExternalForm());
       c=new MediaPlayer(mediaFondo);
       c.setAutoPlay(true);
       MediaView mediaView=new MediaView(c);
   }
   public void playIntro(){
       //reproduzco la musica de fondo en los botones
       mediaIntro=new Media(Memoria.class.getResource("/musica/SuperSmashBros-043.wav").toExternalForm());
       MediaPlayer p=new MediaPlayer(mediaIntro);
       p.setAutoPlay(true);
       MediaView mediaView=new MediaView(p);
   }
   public void playCarta(){
    mediaIntro=new Media(Memoria.class.getResource("/musica/SuperSmashBros-193.wav").toExternalForm());
    MediaPlayer p=new MediaPlayer(mediaIntro);
    p.setAutoPlay(true);
    MediaView mediaView=new MediaView(p);
}
   public void stop(){
       c.stop();
   }
   public void playGanar(){
    mediaFondo=new Media(Memoria.class.getResource("/musica/SuperSmashBros-003.wav").toExternalForm());
    c=new MediaPlayer(mediaFondo);
    c.setAutoPlay(true);
    MediaView mediaView=new MediaView(c);
   }
}
